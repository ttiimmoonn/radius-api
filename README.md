<h1> Radius-api </h1>
Python 3.5. or greater
Modules are required: aiohttp, cchardet, aiodns, aiosqlite.

<h2> [ru] </h2>
Фреймворк для взаимодействия с freeradius сервером. Фреймворк служит для получения, записи и редактирования данных из базы sqlite через http запросы.
<h3> Файловая структура </h3>
Этот сервер использует для запуска файл:
<li> server-main.py </li>
Файл с конфигурацией хранится по пути: <code>/config/radius-api-conf.json</code>. 

<h3>Конфигурационный файл:</h3>
<h4>Блок с настройкой параметров сервера:</h4>
<li> <code> "host": "localhost" </code>, <code> - host </code> Хост который будет прослушиваться. </li>

<li> <code> "port": "8082" </code>, <code> - port </code> Порт который будет прослушиваться. </li>

<h4>Блок с настройками базы данных <code>"database"</code>:</h4>
<li> <code> "database-path": "/home/username/sqlite_rad.db" </code>, <code> - database-path </code> Путь до db sqlite freeradius сервера.  </li>

<h3> API: </h3>
Методы API рассчитаны для взаимодействия с SIP пользователями freeradius. Сервер использует следующие методы API:

<h4>Получить полный список всех пользователей в базе данных:</h4>

**url:**<code>/api/user/list</code>

**Метод:** <code>GET</code>
```json
response:
[
    "<username>",
    ...
    "<username>"
]
```
<h4>Получить все параметры пользователя по {username}:</h4>

**url:** <code>/api/user/{username}/info</code>

**Метод:** <code>GET</code>
```json
response:
{
    "username": "<username>",
    "properties":
        [
            {
                "name": "<property_name>",
                "comparison": "<comparison_value>",
                "value": "<value>"
            },
            ...
            {
                "name": "<property_name>",
                "comparison": "<comparison_value>",
                "value": "<value>"
            } 
        ]
}
```

<h4>Добавить пользователя и параметры в базу данных:</h4>

**url:** <code>/api/user/{username}/set</code>

**Метод:** <code>POST</code>
```json
request:
{
    "username": "<username>",
    "properties":
        [
            {
                "name": "<property_name>",
                "comparison": "<comparison_value>",
                "value": "<value>"
            },
            ...
            {
                "name": "<property_name>",
                "comparison": "<comparison_value>",
                "value": "<value>"
            } 
        ]
}
response:
200 - при успехе;
501 - при ошибки записи;
```

<h3>Пример использования:<h3>
<h4>Добавим в базу данных radius сервера пользователя с именем 101 и паролем 123:</h4>

```json
$curl -d '{"username":"101","properties":[{"name":"Cleartext-Password","comparison":":=","value":"123"}]}' -H 'Content-Type: application/json' 10.0.5.48:8082/api/user/set
```

<h4>Выведем список всех пользователей:</h4>

```json
$curl -G 10.0.5.48:8082/api/user/list

["101"]
```

<h4>Выведем полную информацию по свойствам пользователя:</h4>

```json
$ccurl -G 10.0.5.48:8082/api/user/101/info

{"username": "101", "properties": [{"name": "Cleartext-Password", "comparison": ":=", "value": "101"}]}
```