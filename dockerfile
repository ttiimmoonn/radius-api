FROM freeradius/freeradius-server:latest

# Задаем переменные
ENV sqlDbPath=/etc/raddb/sqlite_rad.db
# ENV http_proxy http://proxy-chain.xxx.com:911/
# ENV https_proxy http://proxy-chain.xxx.com:912/
#

# Директория для приложения
RUN mkdir -p /python/src/app
# Директория для логов
RUN mkdir -p /python/src/app/log


# Определяем рабочую папку
WORKDIR /python/src/app

# Копируем все текущие файлы в рабочую директорию
COPY . /python/src/app

# Устанавливаем pip
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y python3-pip sqlite3

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

# Конфигурация freeradius сервера
# Создаем базу sqlite3
RUN bash -c 'sqlite3 /etc/raddb/sqlite_rad.db < /etc/freeradius/mods-config/sql/main/sqlite/schema.sql'
# Подключаем sql модуль
RUN ln -s /etc/freeradius/mods-available/sql /etc/freeradius/mods-enabled/sql
RUN sed -i "s%driver = \"rlm_sql_null\"%$sqlConfig%g" /etc/freeradius/mods-enabled/sql

EXPOSE 8082
EXPOSE 1812
EXPOSE 1813

CMD ["python3", "server-main.py"]
