from dotmap import DotMap
from aiohttp import web
import aiosqlite
import json

class Handler:
    def __init__(self, logger, db_path):
        self.db_path = db_path
        self.logger = logger

    async def userList(self, request):
        answer = []

        try:
            async with aiosqlite.connect(self.db_path) as db:
                async with db.execute('SELECT * FROM radcheck') as cursor:
                    results = await cursor.fetchall()
        except Exception as e:
            self.logger.error('Error get all users from db.')
            self.logger.error(e)
            return web.StreamResponse(status=500, reason='Server errors')
        
        if (results):
            for res in results:
                answer.append(str(res[1]))
            answer = list(set(answer))
        return web.json_response(answer)


    async def userInfo(self, request):
        answer = {}
        username = request.match_info.get('username', "None")

        try:
            async with aiosqlite.connect(self.db_path) as db:
                self.logger.info("Select in db: \"SELECT * FROM radcheck WHERE username = '{}'\"".format(username))
                async with db.execute("SELECT * FROM radcheck WHERE username = '{}'".format(username)) as cursor:
                    results = await cursor.fetchall()
        except Exception as e:
            self.logger.error('Error get user from db.')
            self.logger.error(e)
            return web.StreamResponse(status=500, reason='Server errors')
        
        if (results):
            answer["username"] = results[0][1]
            answer["properties"] = []
            for res in results:
                answer["properties"].append({"name": res[2], "comparison": res[3], "value": res[1]})
        return web.json_response(answer)


    async def addUser(self, request):
        answer = {}
        userDb = {"username": "", "properties": []}
        coincidence = False
        updateList = []

        try:
            self.logger.info('Get and validate json from request...')
            data = await request.json()
            self.logger.debug('Value from json: {}'.format(data))
        except Exception as e:
            self.logger.error('Error get data from message.')
            self.logger.error(e)
            return web.StreamResponse(status=501, reason='Error data from message')

        # Получаем список всех существующих параметров по пользователю
        try:
            self.logger.info('We are looking for a user in the database...')
            async with aiosqlite.connect(self.db_path) as db:
                self.logger.debug("Select in db: \"SELECT * FROM radcheck WHERE username = '{}'\"".format(data["username"]))
                async with db.execute("SELECT * FROM radcheck WHERE username = '{}'".format(data["username"])) as cursor:
                    results = await cursor.fetchall()
                    self.logger.debug('User in the database: {}'.format(results))
        except Exception as e:
            self.logger.error('Error get user from db.')
            self.logger.error(e)
            return web.StreamResponse(status=500, reason='Server errors')

        if (results):
            userDb["username"] = results[0][1]
            userDb["properties"] = []
            for res in results:
                userDb["properties"].append({"name": res[2], "comparison": res[3], "value": res[1]})

        # Заменяем и добавляем параметры
        try:
            self.logger.info('Write new data to database...')
            async with aiosqlite.connect(self.db_path) as db:    
                for prospRequest in data["properties"]:
                    for prospUserDb in userDb["properties"]:
                        if (prospRequest["name"] == prospUserDb["name"]):
                            async with db.execute(
                                "UPDATE radcheck SET value = '{}', op = '{}' WHERE username = '{}' AND attribute = '{}'".format(
                                    prospRequest["value"],
                                    prospRequest["comparison"],
                                    data["username"],
                                    prospRequest["name"]
                                    )
                                ) as cursor:
                                await db.commit()
                            coincidence = True
                    if not coincidence:
                        updateList.append(prospRequest)
                    coincidence = False

                for prospRequest in updateList:
                    async with db.execute(
                        "INSERT INTO radcheck (username,attribute,op,value) VALUES ('{}','{}','{}','{}');".format(
                            data["username"],
                            prospRequest["name"],
                            prospRequest["comparison"],
                            prospRequest["value"]
                            )
                        ) as cursor:
                        await db.commit()
        except Exception as e:
            self.logger.error('Error record user from db.')
            self.logger.error(e)
            return web.StreamResponse(status=500, reason='Server errors')     
        
        return web.StreamResponse(status=200) 

'''

        'UPDATE radcheck SET {} = '{}' WHERE username = '{}';'
        UPDATE radcheck SET value = '99' WHERE username = 'testtest' AND attribute = 'password'

        props = []

        try:
            async with aiosqlite.connect(self.db_path) as db:
                self.logger.info("Select in db: \"SELECT * FROM radcheck WHERE username = '{}'\"".format(username))
                async with db.execute("SELECT * FROM radcheck WHERE username = '{}'".format(username)) as cursor:
                    results = await cursor.fetchall()
        except Exception as e:
            self.logger.error('Error get user from db.')
            self.logger.error(e)

        if (results):
            for res in results:
                props.append(res[2])
        else:    

        {
            "username": "<username>",
            "properties":
                [
                    {
                        "name": "<property_name>",
                        "comparison": "<comparison_value>",
                        "value": "<value>"
                    },
                    ...
                    {
                        "name": "<property_name>",
                        "comparison": "<comparison_value>",
                        "value": "<value>"
                    } 
                ]
        }            
'''