#!/bin/bash

#
# Скрипт для разворачивания радиус сервера в lxc контейнере. Скрипт создает контейнер, создает среду, подключает sqlite 
# к радиус серверу.
#

sqlDbPath="/etc/raddb/sqlite_rad.db"
radiusApiPath="/etc/radiusApi"
sqlConfig="driver = \"rlm_sql_sqlite\"\\n\\n        sqlite {\\n		filename = \"$sqlDbPath\"\\n	}\\n"

start ()
{
  echo "Запускаем контейнер."
  sudo lxc-start -n radius
}

create ()
{
  echo "Создание контейнера для radius."
  sudo lxc-create -t download -n radius -- --dist ubuntu --release bionic --arch amd64
  start
}

stop ()
{
  echo "Останавливаем контейнер."
  sudo lxc-stop -n radius
}

destroy ()
{
  echo "Удаляем контейнер."
  stop
  sudo lxc-destroy -n radius
}

config ()
{
  echo "Устанавливаем freeradius."
  sudo lxc-attach -n radius -- apt install freeradius -y
  sudo lxc-attach -n radius -- apt install sqlite3 -y
  sudo lxc-attach -n radius -- apt install git -y
  sudo lxc-attach -n radius -- systemctl status freeradius

  echo "Создадим базу данных."
  sudo lxc-attach -n radius -- mkdir /etc/raddb
  sudo lxc-attach -n radius -- bash -c 'sqlite3 /etc/raddb/sqlite_rad.db < /etc/freeradius/3.0/mods-config/sql/main/sqlite/schema.sql'

  echo "Подключаем модуль sql."
  sudo lxc-attach -n radius -- ln -s /etc/freeradius/3.0/mods-available/sql /etc/freeradius/3.0/mods-enabled/sql
  
  echo "Настраиваем модуль sql."
  sudo lxc-attach -n radius -- sed -i "s%driver = \"rlm_sql_null\"%$sqlConfig%g" /etc/freeradius/3.0/mods-enabled/sql

  echo "Перезагружаем radius."
  sudo lxc-attach -n radius -- systemctl restart freeradius
  sudo lxc-attach -n radius -- systemctl status freeradius

  echo "Забираем radius-api сервер"
  sudo lxc-attach -n radius -- mkdir /etc/raddb
  sudo lxc-attach -n radius -- git clone https://gitlab.com/ttiimmoonn/radius-api.git $radiusApiPath
  sudo lxc-attach -n radius -- sed -i "s%\"database-path\": \"/home/ttiimmoonn/sqlite_rad.db\"%\"database-path\": \"/home/ttiimmoonn/sqlite_rad.db\"g" ${radiusApiPath}/config/radius-api-conf.json
}

help_ ()
{
	echo -e 'create - создание контейнера \nstart - запуск контейнера \nstop - остановка контейнера \ndestroy - удаление контейнера \ninfo - информация о контейнере \nconfig - загрузка freeradius and sqlite'
}

info ()
{
  sudo lxc-ls -f
}
	
while [ -n "$1" ]
	do
	case "$1" in
	-h) help_ ;;
	--help) help_ ;;
	--create) create ;;
	--start) start ;;
	--stop) stop ;;
	--destroy) destroy ;;
	--config) config ;;
	--info) info ;;
	esac
	shift
done
